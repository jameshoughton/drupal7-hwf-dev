<div class="product-l-col">
    <?php print render($content['field_product_image'])?>
</div>

<div class="product-r-col">
    <h1><?php print render($title)?></h1>
    <?php print render($content['body'])?>
    <h3>Specification</h3>
    <?php print render($content['field_species'])?>
    <?php print render($content['field_type'])?>
    <?php print render($content['field_width'])?>
    <?php print render($content['field_thickness'])?>
    <?php print render($content['field_length'])?>
    <?php print render($content['field_finish'])?>
</div>
