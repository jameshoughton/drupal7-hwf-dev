<div<?php print $attributes; ?>>
    <header class="l-header" role="banner">
        <div class="l-branding">
            <?php if ($logo): ?>
                <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" class="site-logo"><img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" /></a>
            <?php endif; ?>

            <?php if ($site_name || $site_slogan): ?>
                <?php if ($site_name): ?>
                    <h1 class="site-name">
                        <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home"><span><?php print $site_name; ?></span></a>
                    </h1>
                <?php endif; ?>

                <?php if ($site_slogan): ?>
                    <h2 class="site-slogan"><?php print $site_slogan; ?></h2>
                <?php endif; ?>
            <?php endif; ?>

            <?php print render($page['branding']); ?>
        </div>

        <?php print render($page['header']); ?>

    </header>
        <div class="l-navigation primary-navigation">
            <navigation role="navigation">
                <?php print render($page['navigation']); ?>
            </navigation>
        </div>

        <div class="l-navigation secondary-navigation">
            <navigation role="aside">
                <?php print render($page['secondary_navigation']); ?>
            </navigation>
        </div>

    <?php if($page['slideshow']) : ?>

            <div class="l-slideshow">
                <aside role="aside">
                    <?php print render($page['slideshow']); ?>
                </aside>
            </div>

    <?php endif; ?>
    <div class="l-content-first">
        <?php print $breadcrumb; ?>
        <?php print render($page['content_first']); ?>
    </div>

    <div class="l-main">
        <div class="l-content" role="main">
            <?php print render($page['highlighted']); ?>
            <?php //print $breadcrumb; ?>
            <a id="main-content"></a>
            <?php print render($title_prefix); ?>
            <?php if ($title): ?>
                <h1><?php print $title; ?></h1>
            <?php endif; ?>
            <?php print render($title_suffix); ?>
            <?php print $messages; ?>
            <?php print render($tabs); ?>
            <?php print render($page['help']); ?>
            <?php if ($action_links): ?>
                <ul class="action-links"><?php print render($action_links); ?></ul>
            <?php endif; ?>
            <?php print render($page['content']); ?>
            <?php print $feed_icons; ?>
        </div>

        <?php print render($page['sidebar_first']); ?>
        <?php print render($page['sidebar_second']); ?>
    </div>

    <?php if($page['product_display']) : ?>
            <aside class="l-product-display" role="aside">
                <?php print render($page['product_display']) ?>
            </aside>

    <?php endif; ?>

    <?php if($page['blog_display']) : ?>
            <aside class="l-product-display blog-display" role="aside">
                <?php print render($page['blog_display']) ?>
            </aside>
    <?php endif; ?>

    <div class="l-newsletter-display">
        <footer class="l-newsletter-display" role="aside">
            <?php print render($page['newsletter_display']) ?>
        </footer>
    </div>



        <footer class="l-footer-first" role="aside">
            <?php print render($page['footer_first']) ?>
        </footer>

        <footer class="l-footer" role="contentinfo">
            <?php print render($page['footer']); ?>
        </footer>

</div>
