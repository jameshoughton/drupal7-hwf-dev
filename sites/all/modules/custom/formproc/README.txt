2014-08-08:
-----------

Added support for fields that get represented as arrays, like for example checkboxes.
The need came from the contact form of the Harrop Consulting website.

2014-07-03:
-----------

Added support for multifile file type, stemmed from module "Webform Multiple File Upload":
https://www.drupal.org/project/webform_multifile

This was required by the Admiral Design and Print project staged at adp.silverdisc.co.uk.
Here is an example of the structure posted to the endpoint, when there are fields of type Multifile
(reference_files in this example):

    [page_title] => Letterheads
    [contact] => SilverDisc
    [last_name] => Testing
    [company_name] => SilverDisc Limited
    [email] => paul@silverdisc.co.uk
    [phone] => +56-2-22244478
    [comments] => test 17
    [reference_files] => Array
        (
            [0] => Array
                (
                    [fid] => 998
                    [uid] => 0
                    [filename] => by-the-sea.jpg
                    [uri] => http://adp.silverdisc.co.uk/sites/default/files/webform/by-the-sea_6.jpg
                    [filemime] => image/jpeg
                    [filesize] => 24741
                    [status] => 1
                    [timestamp] => 1404403456
                    [uuid] => 551d0f51-d411-451d-b1f7-3d496022f027
                )

            [1] => Array
                (
                    [fid] => 999
                    [uid] => 0
                    [filename] => by-the-sea-square.jpg
                    [uri] => http://adp.silverdisc.co.uk/sites/default/files/webform/by-the-sea-square_8.jpg
                    [filemime] => image/jpeg
                    [filesize] => 13077
                    [status] => 1
                    [timestamp] => 1404403456
                    [uuid] => 31f4c308-28fd-4afb-aa87-44c2fbb0a533
                )

        )

    [siteId] => 414
    [siteurl] => http://adp.silverdisc.co.uk